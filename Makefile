LIBS=-lcppcms -lbooster -lcppdb

all: uploader

uploader: tmpl
	$(CXX) $(CXXFLAGS) -Wall ./apps/main.cpp ./apps/photo.cpp ./output/upload.cpp -o ./output/uploader ${LIBS}

tmpl: ./view/images.tmpl ./view/index.tmpl ./view/upload.tmpl ./view/app.tmpl
	cppcms_tmpl_cc ./view/images.tmpl ./view/index.tmpl ./view/upload.tmpl ./view/app.tmpl -o ./output/upload.cpp

clean:
	rm -fr ./output/uploader ./output/upload.cpp

