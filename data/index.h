#ifndef DATA_INDEX_H
#define DATA_INDEX_H

#include <string>
#include <cppcms/view.h>

namespace data {
	struct index : public cppcms::base_content {
		std::string basePath;
	};
};

#endif