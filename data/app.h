#ifndef DATA_APP_H
#define DATA_APP_H

#include <string>
#include <cppcms/view.h>

namespace data {
	struct app : public cppcms::base_content {
		std::string basePath;
		std::string ip;
		std::string port;
	};
};

#endif