#ifndef PHOTO_H
#define PHOTO_H

#include <string>

class photo {
	public:
		photo();
		virtual ~photo();
		std::string id;
		std::string url;
		std::string name;
		std::string upload_time;
		std::string size;
		std::string upload_method;
		std::string description;
};

#endif