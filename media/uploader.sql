/*
 Navicat Premium Data Transfer

 Source Server         : 10.211.55.4
 Source Server Type    : MySQL
 Source Server Version : 50540
 Source Host           : 10.211.55.4
 Source Database       : uploader

 Target Server Type    : MySQL
 Target Server Version : 50540
 File Encoding         : utf-8

 Date: 01/12/2015 21:44:18 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `t_photo`
-- ----------------------------
DROP TABLE IF EXISTS `t_photo`;
CREATE TABLE `t_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `t_photo`
-- ----------------------------
BEGIN;
INSERT INTO `t_photo` VALUES ('1', 'http://10.211.55.4:8080/photos/1421032925625.jpg', 'b7fd5266d01609243dbcd103d60735fae6cd3408.jpg', '2015年1月12日 11:22:5', '169203', null), ('2', 'http://10.211.55.4:8080/photos/1421033109121.jpg', 'c75c10385343fbf24027fc22b27eca8065388fac.jpg', '2015å¹´1æœˆ12æ—¥ 11:25:9', '218645', null), ('5', 'http://10.211.55.4:8080/photos/1421034598667.jpg', 'c75c10385343fbf24027fc22b27eca8065388fac.jpg', '2015年1月12日 11:49:58', '218645', null), ('6', 'http://10.211.55.4:8080/photos/1421045571814.jpg', 'c75c10385343fbf24027fc22b27eca8065388fac.jpg', '2015年1月12日 14:52:51', '218645', null), ('7', 'http://10.211.55.4:8080/photos/1421061485070.jpeg', '2.jpg', '2015年1月12日 19:18:05', '287145', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
